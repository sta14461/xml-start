package at.stalzer.europaeischezentralbank;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

import at.spenger.xml.DefaultConfig;
import at.spenger.xml.shiporder.Item;
import at.spenger.xml.shiporder.StaXParser;
import at.spenger.xml.xpath.XPathReader;

@Import(DefaultConfig.class)
public class Application implements CommandLineRunner {

	public static void main(String[] args) {
		 new SpringApplicationBuilder(Application.class)
		    .showBanner(false)
		    .logStartupInfo(false)
		    .run(args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		readEUXML();
		

	}

	private void readEUXML() throws IOException {
		InputStream in = new ClassPathResource("eubank.xml")
				.getInputStream();
		KursXMLParser p = new KursXMLParser();
		if(in==null)
		{
			System.out.println("FEHLER");
		}
		List<Kurs> kurse = p.read(in);

		for (Kurs kurs : kurse) {
			System.out.println(kurs);
			
			
			
		}
	}
}
