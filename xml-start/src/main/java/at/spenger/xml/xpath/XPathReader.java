package at.spenger.xml.xpath;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XPathReader {
	/**
	 * Evaluate an XML input stream using the given xpath.
	 * 
	 * @param in
	 *            The XML input stream.
	 * @param xpathExpr
	 *            The xpath expression - must not contain any namespace
	 *            prefixes.
	 * @param result
	 *            A collection to append the results to.
	 */
	private void eval(InputStream in, String xpathExpr, Collection<String> result)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException {

		// Standard of reading an XML file
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(false); // important!
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(in);

		// create an XPathFactory
	    XPathFactory xFactory = XPathFactory.newInstance();

	    // create an XPath object
	    XPath xpath = xFactory.newXPath();

	    // Compile the XPath expression
		XPathExpression expr = xpath.compile(xpathExpr);

		// Run the query and get a nodeset
		NodeList nodeList = (NodeList) expr.evaluate(doc,
				XPathConstants.NODESET);
		
		for (int i = 0; i < nodeList.getLength(); ++i) {
			Node node = nodeList.item(i);
			result.add(node.getNodeValue());
		}
	}

	/**
	 * Read the titles of entries from the Heise news feed.
	 */
	public void readHeiseFeed() throws Exception {
		URL heiseFeed = new URL(
				"http://heise.de.feedsportal.com/c/35207/f/653902/index.rss");
		InputStream in = heiseFeed.openStream();

		Collection<String> result = new LinkedList<String>();
		eval(in, "//item/title/text()", result);
		printResult(result, System.out);
	}
	
	





	/**
	 * Print the result.
	 * 
	 * @param result
	 *            The collection of results.
	 * @param out
	 *            The print stream to use.
	 */
	public void printResult(Collection<String> result, PrintStream out) {
		// print result
		for (String name : result) {
			out.println(name);
		}
	}


}
